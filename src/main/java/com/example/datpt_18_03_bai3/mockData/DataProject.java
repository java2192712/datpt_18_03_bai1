package com.example.datpt_18_03_bai3.mockData;

import com.example.datpt_18_03_bai3.manager.Student;
import com.example.datpt_18_03_bai3.manager.Teacher;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@Component
public class DataProject {
    private Student studentAll = new Student("Dat", 22, "nam", "61TH1", 6.0f );
    private Student student1 = new Student("Dat", 22, "nam" );
    private Student student2 = new Student("61TH1", 10.0f);
    private Student student = new Student();

    private Teacher teacherAll = new Teacher("Dat", 22, "nam", 2.5f, "Hẹn hò" );
    private Teacher teacher1 = new Teacher("Dat", 22, "nam" );
    private Teacher teacher2 = new Teacher(2.0f, "Độc thân");
    private Teacher teacher = new Teacher();
}
