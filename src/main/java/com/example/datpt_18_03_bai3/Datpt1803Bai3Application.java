package com.example.datpt_18_03_bai3;

import com.example.datpt_18_03_bai3.manager.Person;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.lang.reflect.Method;

@SpringBootApplication
public class Datpt1803Bai3Application {

	public static void main(String[] args) {
		SpringApplication.run(Datpt1803Bai3Application.class, args);
	}

}
