package com.example.datpt_18_03_bai3.controller;

import com.example.datpt_18_03_bai3.manager.Student;
import com.example.datpt_18_03_bai3.responAPI.ResponseApi;
import com.example.datpt_18_03_bai3.service.PersonService;
import com.example.datpt_18_03_bai3.service.StudentService;
import com.example.datpt_18_03_bai3.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/person")
public class PersonController {
    @Autowired
    private StudentService studentService;

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private PersonService personService;

    @GetMapping("/getStudent")
    public ResponseEntity<ResponseApi> getStudent(){
        return new ResponseEntity<>(studentService.getStudent(), HttpStatus.OK);
    }

    @GetMapping("/getTeacher")
    public ResponseEntity<ResponseApi> getTeacher(){
        return new ResponseEntity<>(teacherService.getTeacher(), HttpStatus.OK);
    }

    @GetMapping("/getPersonAll")
    public ResponseEntity<ResponseApi> getPersonAll(){
        return new ResponseEntity<>(personService.getPersonAll(), HttpStatus.OK);
    }

    @GetMapping("/getStudentPrintHello")
    public ResponseEntity<ResponseApi> getStudentPrintHello(){
        return new ResponseEntity<>(studentService.getStudentPrintHello(), HttpStatus.OK);
    }

    @GetMapping("/getTeacherPrintHello")
    public ResponseEntity<ResponseApi> getTeacherPrintHello(){
        return new ResponseEntity<>(teacherService.getTeacherPrintHello(), HttpStatus.OK);
    }

}
