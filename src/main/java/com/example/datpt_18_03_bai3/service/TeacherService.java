package com.example.datpt_18_03_bai3.service;

import com.example.datpt_18_03_bai3.manager.Student;
import com.example.datpt_18_03_bai3.manager.Teacher;
import com.example.datpt_18_03_bai3.mockData.DataProject;
import com.example.datpt_18_03_bai3.responAPI.ResponseApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TeacherService {
    @Autowired
    private DataProject dataProject;

    public ResponseApi getTeacher(){
        List<Teacher> content = new ArrayList<>();
        content.add(dataProject.getTeacherAll());
        content.add(dataProject.getTeacher());
        content.add(dataProject.getTeacher1());
        content.add(dataProject.getTeacher2());
        return new ResponseApi(content);
    }
    public ResponseApi getTeacherPrintHello(){
        List content = new ArrayList<>();
        content.add(dataProject.getTeacher().printHello());
        content.add(dataProject.getTeacher().printHello("Dat", 22));
        content.add(dataProject.getTeacher().printHello(22));
        Teacher teacher = new Teacher();
        teacher.hello();
        return new ResponseApi(content);
    }
}
