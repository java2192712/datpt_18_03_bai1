package com.example.datpt_18_03_bai3.service;

import com.example.datpt_18_03_bai3.manager.Student;
import com.example.datpt_18_03_bai3.mockData.DataProject;
import com.example.datpt_18_03_bai3.responAPI.ResponseApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class StudentService {

    @Autowired
    private DataProject dataProject;
    public ResponseApi getStudent(){
        List<Student> content = new ArrayList<>();
        content.add(dataProject.getStudentAll());
        content.add(dataProject.getStudent());
        content.add(dataProject.getStudent1());
        content.add(dataProject.getStudent2());
        return new ResponseApi(content);
    }

    public ResponseApi getStudentPrintHello(){
        List content = new ArrayList<>();
        content.add(dataProject.getStudent().printHello());
        content.add(dataProject.getStudent().printHello("Dat", 22));
        content.add(dataProject.getStudent().printHello(22));
        Student student = new Student();
        student.hello();
        student.hello("Dat");
        return new ResponseApi(content);
    }

}
