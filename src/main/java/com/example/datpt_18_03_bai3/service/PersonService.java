package com.example.datpt_18_03_bai3.service;

import com.example.datpt_18_03_bai3.manager.Person;
import com.example.datpt_18_03_bai3.manager.Student;
import com.example.datpt_18_03_bai3.manager.Teacher;
import com.example.datpt_18_03_bai3.mockData.DataProject;
import com.example.datpt_18_03_bai3.responAPI.ResponseApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PersonService {
    @Autowired
    private DataProject dataProject;

    public ResponseApi getPersonAll(){
        List<Person> content = new ArrayList<>();
        content.add(dataProject.getStudentAll());
        content.add(dataProject.getStudent());
        content.add(dataProject.getStudent1());
        content.add(dataProject.getStudent2());

        content.add(dataProject.getTeacherAll());
        content.add(dataProject.getTeacher());
        content.add(dataProject.getTeacher1());
        content.add(dataProject.getTeacher2());
        return new ResponseApi(content);
    }

}
