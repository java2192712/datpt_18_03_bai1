package com.example.datpt_18_03_bai3.manager;

import org.springframework.stereotype.Component;

@Component
public class Student extends Person {
    private String lop;
    private float diem;

    public String getLop() {
        return lop;
    }

    public void setLop(String lop) {
        this.lop = lop;
    }

    public float getDiem() {
        return diem;
    }

    public void setDiem(float diem) {
        this.diem = diem;
    }

    public Student(String ten, int tuoi, String gioiTinh, String lop, float diem) {
        super(ten, tuoi, gioiTinh);
        //super là 1 biến tham chiếu đến thuộc tính của class cha Person gần nhất và sẽ thay giá trị
        // nghĩa là khi một đối tượng Student được tạo, trước tiên chương trình sẽ khởi tạo đối tượng Person
        // tương ứng với các giá trị tên, tuổi, và giới tính được truyền vào
        this.lop = lop;
        this.diem = diem;
    }
    public Student(String ten, int tuoi, String gioiTinh) {
        super(ten, tuoi, gioiTinh);
    }
    public Student(String lop, float diem) {
        this.lop = lop;
        this.diem = diem;
    }
    public Student() {
    }

    @Override
    public String printHello() {
        return "Hello, I am a Student";
    }

    // đây không phải là Override do class preson để hàm là private chỉ là tạo ra phương thức trùng tên bên class con
    public void hello(){
        System.out.println("Học sinh ngoan");
    }

    public void hello(String ten){
        System.out.println(ten + "Là 1 học sinh ngoan");
    }

    public String printHello(String ten, int tuoi) {
        return "Hello, I am a student, my name: " + ten + " i am " + tuoi + " years old";
    }

    public String printHello(int tuoi) {
        return  "Hello, I am a student, i am " + tuoi + " years old";
    }

}
